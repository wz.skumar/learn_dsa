// The Binary Search algorithm is used to search for any element in a sorted array.
// If the element is found, it returns the element’s index. If not, it returns -1.

// With Binary Search we:

// 1. Start in the middle and check if the target is greater or less than that middle value.
// 2. If the target is greater than the middle value, we will next look at the second half of the array (ignore the left side)
// 3. If the target is smaller, we look at the first half of the array (ignore the right side).
// 4. We pick the middle of that half and check if it’s greater or less than our target.
// 5. Repeat this process until we find our target.

// implementation of binary search
function binarySearch(arr, target) {
  let start = 0;
  let end = arr.length - 1;
  while (start <= end) {
    let middle = Math.floor((start + end) / 2);
    if (arr?.[middle] == target) {
      return middle;
    } else if (arr?.[middle] < target) {
      start = middle + 1;
    } else {
      end = middle - 1;
    }
  }
  return -1;
}
console.log(binarySearch([1, 2, 3, 4], 1)); // 1
console.log(binarySearch([1, 2, 3, 5, 9], 4)); // -1
console.log(binarySearch([1, 2, 3, 4, 5], 5)); // 4
console.log(binarySearch([0, 3], 3)); // 1

//   Binary Search time complexity

// Best case complexity of Binary Search
// The best case complexity of Binary Search occurs when the first comparison is correct (the target value is in the middle of the input array).
// This means that regardless of the size of the array, we’ll always get the result in constant time.
// Therefore, the best case time complexity is O(1) - constant time.

// Worst case complexity of Binary Search
// The worst case complexity of Binary Search occurs when the target value is at the beginning or end of the array.

//  if we have an array 32 elements long and our target is 32, then the array will be divided five times until we find 32. So,
//  the Big O complexity of binary search is O(log(n)) – logarithmic time complexity: log(32) = 5.

// Average case complexity of Binary Search
// The average case is also of O(log(n)).

// Space complexity of Binary Search
// Binary Search requires three pointers to elements (start, middle and end), regardless of the size of the array.
// Therefore the space complexity of Binary Search is O(1) – constant space.

// Time Complexity (Best)	O(1)
// Time Complexity (Average)	O(log(n))
// Time Complexity (Worst)	O(log(n))
// Space Complexity	O(1)
